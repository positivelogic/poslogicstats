﻿using System.Threading.Tasks;
using Moq;

using PoslogicStats.Repositories.Credential;

namespace PoslogicStats.Tests.Mocks.Repositories
{
    internal class MoqCredentialRepository :
        Mock<ICredentialRepository>
    {
        public string GetApiKeysResults = "";
        public MoqCredentialRepository()
        {
            Setup(m => m.GetApiKeysAsync()).Returns(() =>
            {
                return Task.FromResult(GetApiKeysResults);
            });
        }
    }
}