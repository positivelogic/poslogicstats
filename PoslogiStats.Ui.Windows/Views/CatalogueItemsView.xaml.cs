﻿using System;
using System.Windows;
using PoslogiStats.Ui.Windows.ViewModels;
using MahApps.Metro.Controls;

namespace PoslogiStats.Ui.Windows.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CatalogueItemsView : MetroWindow
    {
        readonly CatalogueVm _catalogueVm;
        public CatalogueItemsView()
        {
            InitializeComponent();
            _catalogueVm = new CatalogueVm();
            DataContext = _catalogueVm;
        }
      

        private async void ButtonGetData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await _catalogueVm.GetCatalogue();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonDetail_Click(object sender, RoutedEventArgs e)
        {
             _catalogueVm.ShowDetails();
        }
    }
}