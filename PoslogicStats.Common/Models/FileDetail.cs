﻿namespace PoslogicStats.Common.Models
{
    public class FileDetail
    {
        public string Path { get; set; }
        public string FileName { get; set; }
    }
}