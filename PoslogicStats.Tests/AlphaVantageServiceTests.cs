﻿using System.IO;
using System.Threading.Tasks;

using NUnit.Framework;
using PoslogicStats.Common.Clients;
using PoslogicStats.Repositories.AlphaVantage;
using PoslogicStats.Repositories.Credential;
using PoslogicStats.Repositories.Setting;
using PoslogicStats.Services.AlphaVantage;
using PoslogicStats.Services.Credential;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class AlphaVantageServiceTests
    {
        //[Test]
        [Category("Integration")]
        public async Task Int_Call_GetDailyDigitalCryptoCurrenciesAsync()
        {
            using (var webResource = new WebApiClient())
            {
                var path = Directory.GetCurrentDirectory();
                var settingRepo = new SettingRepository(path);
                var fileResource = new FileSystemClient();
                var alphaVantageRepo = new AlphaVantageRepository(webResource, fileResource, settingRepo);
                var credentialRepo = new CredentialRepository(fileResource, settingRepo);
                var credentialService = new CredentialService(credentialRepo);

                var target = new AlphaVantageService(alphaVantageRepo, credentialService);

                var actual = await target.GetDailyDigitalCryptoCurrenciesAsync("BIT", "NZD");

                Assert.IsNotNull(actual);
            }
        }
    }
}
