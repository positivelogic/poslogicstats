﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using PoslogicStats.Repositories.Credential;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Services.Credential
{
    public interface ICredentialService
    {
        Task<IEnumerable<ApiKey>> GetApiKeyAsync();
        Task<bool> PutApiKeyAsync(ApiKey value, bool update = false);
    }

    public class CredentialService : ICredentialService
    {
        readonly ICredentialRepository _credentialRepository;
        public CredentialService(ICredentialRepository credentialRepository)
        {
            _credentialRepository = credentialRepository;
        }

        public async Task<IEnumerable<ApiKey>> GetApiKeyAsync()
        {
            var stringResult = await _credentialRepository.GetApiKeysAsync();
            var keys = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ApiKey>>(stringResult);
            return keys;
        }

        public async Task<bool> PutApiKeyAsync(ApiKey value, bool update = false)
        {
            if (value == null)
                throw new ArgumentNullException("ApiKey value");

            var stringResult = await _credentialRepository.GetApiKeysAsync();
            var keys = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ApiKey>>(stringResult);

            var templist = new List<ApiKey>();

            if (keys != null && keys.Count() > 0)
            {
                templist.AddRange(keys);

                if (templist.Contains(value))   
                {
                    if (update)                 //if key exists in collection, update
                    {
                        var index = templist.IndexOf(value);
                        templist[index] = value;
                    }
                    else                        //if key exists in collection, and you did not specify your intent to update
                    {
                        throw new Exception($"Attempting to update key '{value.Name}' without setting update flag. Please set update flag if the intent is to update this key.");
                    }
                }
                else                            //key does not exists in collection, add it
                {
                    templist.Add(value);
                }
            }
            else
            {
                //brand new collection, first key to be added.
                templist.Add(value);
            }

            var payload = Newtonsoft.Json.JsonConvert.SerializeObject(templist);
            await _credentialRepository.PutApiKeysAsync(payload);

            return true;
        }
    }
}