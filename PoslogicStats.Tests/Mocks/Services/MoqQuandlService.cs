﻿using System.Threading.Tasks;
using Moq;

using PoslogicStats.Services.Quandl;
using PoslogicStats.Common.Models.Quandl;

namespace PoslogicStats.Tests.Mocks.Services
{
    internal class MoqQuandlService
        :Mock<IQuandlService>
    {
        public DatasetsV3 GetDatasetsAsyncResults = new DatasetsV3();
        public TimeSeries GetTimeSeriesDataAsyncResults = new TimeSeries();
        public MoqQuandlService()
        {
            Setup(m => m.GetDatasetsAsync()).Returns(() =>
            {
                return Task.FromResult(GetDatasetsAsyncResults);
            });

            Setup(m => m.GetTimeSeriesDataAsync(It.IsAny<string>(),It.IsAny<string>())).Returns(()=>
            {
                return Task.FromResult(GetTimeSeriesDataAsyncResults);
            });
        }
    }
}