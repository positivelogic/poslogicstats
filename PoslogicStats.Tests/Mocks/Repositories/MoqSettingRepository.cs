﻿using Moq;
using PoslogicStats.Common.Models;
using PoslogicStats.Repositories.Setting;

namespace PoslogicStats.Tests.Mocks.Repositories
{
    internal class MoqSettingRepository :
        Mock<ISettingRepository>
    {
        public FileDetail GetApiFileDetailResult = new FileDetail();
        public MoqSettingRepository()
        {
            Setup(m => m.GetApiFileDetail()).ReturnsAsync(() =>
            {
                return GetApiFileDetailResult;
            });
        }
    }
}