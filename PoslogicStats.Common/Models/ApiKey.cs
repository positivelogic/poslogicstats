﻿namespace PoslogicStats.Common.Models
{
    public class ApiKey
    {
        public string Provider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}