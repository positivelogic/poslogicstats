﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PoslogicStats.Common.Clients;
using PoslogicStats.Common.Models;
using PoslogicStats.Common.Models.AlphaVantage;
using PoslogicStats.Repositories.Setting;

namespace PoslogicStats.Repositories.AlphaVantage
{
    public interface IAlphaVantageRepository
    {
        Task<string> GetDigitalCryptoCurrenciesAsync(IEnumerable<RequestHeader> requestHeaders, DigitalCurrencyFunction digitalCurrencyFunction, string symbol, string market);
        Task<string> GetDigitalCurrencyAsync();
        Task<string> GetPhysicalCurrencyAsync();
    }

    public class AlphaVantageRepository : IAlphaVantageRepository
    {
        readonly IWebApiResourceClient _webResource;
        readonly IFileResourceClient _fileResource;
        readonly ISettingRepository _settingRepository;
        readonly string _url;
        public AlphaVantageRepository(IWebApiResourceClient webResource, IFileResourceClient fileResource, ISettingRepository settingRepository)
        {
            _url = "https://www.alphavantage.co";
            _webResource = webResource;
            _fileResource = fileResource;
            _settingRepository = settingRepository;
        }

        public async Task<string> GetDigitalCryptoCurrenciesAsync(IEnumerable<RequestHeader> requestHeaders, DigitalCurrencyFunction digitalCurrencyFunction, string symbol, string market)
        {
            var requestheader = requestHeaders.FirstOrDefault();
            if (requestheader == null)
                throw new ArgumentException("Request headers not present");

            //https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=BTC&market=CNY&apikey=demo

            var api = $"/query?function={digitalCurrencyFunction.ToString()}&symbol={symbol}&market={market}&{requestheader.Name}={requestheader.Value}";
            return await _webResource.GetAsync(_url, api, requestHeaders);
        }

        public async Task<string> GetDigitalCurrencyAsync()
        {
            var path = await _settingRepository.GetDataLocation();
            var filename = "digital_currency_list.csv";
            return await _fileResource.GetAsync(path, filename);
        }

        public async Task<string> GetPhysicalCurrencyAsync()
        {
            var path = await _settingRepository.GetDataLocation();
            var filename = "physical_currency_list.csv";
            return await _fileResource.GetAsync(path, filename);
        }
    }
}