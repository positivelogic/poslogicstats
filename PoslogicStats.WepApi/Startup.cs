﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PoslogicStats.Repositories.StatisticsNz;
using PoslogicStats.Common.Clients;
using PoslogicStats.Services.Credential;
using PoslogicStats.Repositories.Credential;
using PoslogicStats.Services.StatisticsNz;
using PoslogicStats.Repositories.Setting;

namespace PoslogicStats.WepApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ISettingRepository, SettingRepository>();

            services.AddTransient<IFileResourceClient, FileSystemClient>();
            services.AddTransient<ICredentialRepository, CredentialRepository>();
            services.AddTransient<ICredentialService, CredentialService>();

            services.AddTransient<IWebApiResourceClient, WebApiClient>();
            services.AddTransient<IStatisticNzRepository, StatisticNzRepository>();
            services.AddTransient<IStatisticNzService, StatisticNzService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
