﻿using System.Collections.Generic;
using System.Threading.Tasks;

using PoslogicStats.Common.Models;

namespace PoslogicStats.Common.Clients
{
    public interface IResourceClient
    {
        Task<string> GetAsync(string baseAddress, string path, IEnumerable<RequestHeader> requestHeaders = null);
        Task<bool> PutAsync(string baseAddress, string path, string payload, IEnumerable<RequestHeader> requestHeaders = null);
    }
}