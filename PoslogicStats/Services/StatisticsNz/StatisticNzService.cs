﻿using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using PoslogicStats.Repositories.StatisticsNz;
using PoslogicStats.Services.Credential;
using PoslogicStats.Common.Models.StatisticsNz;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Services.StatisticsNz
{
    public interface IStatisticNzService
    {
        Task<Catalogue> GetCatalogueAsync();
        Task<TableCode> GetByTableCodeAsync(string tableCode);
    }

    public class StatisticNzService : IStatisticNzService
    {
        readonly IStatisticNzRepository _statisticNnzRepository;
        readonly ICredentialService _credentialService;
        const string provider = "StatisticNz";
        public StatisticNzService(IStatisticNzRepository statisticNnzRepository, ICredentialService credentialService)
        {
            _statisticNnzRepository = statisticNnzRepository;
            _credentialService = credentialService;
        }

        public async Task<Catalogue> GetCatalogueAsync()
        {
            ApiKey key = await GetApiKey();
            List<RequestHeader> requestHeaders = SetRequestHeaders(key);

            var stringResult = await _statisticNnzRepository.GetCatalogueAsync(requestHeaders);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Catalogue>(stringResult);
            return result;
        }

        public async Task<TableCode> GetByTableCodeAsync(string tableCode)
        {
            ApiKey key = await GetApiKey();
            List<RequestHeader> requestHeaders = SetRequestHeaders(key);

            var stringResult = await _statisticNnzRepository.GetByTableCodeAsync(tableCode, requestHeaders);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<TableCode>(stringResult);
            return result;
        }

        private async Task<ApiKey> GetApiKey()
        {
            var apikey = "Ocp-Apim-Subscription-Key";
            var keys = await _credentialService.GetApiKeyAsync();
            var key = keys.SingleOrDefault(m => m.Provider.Equals(provider) && m.Name.Equals(apikey));
            return key;
        }

        private static List<RequestHeader> SetRequestHeaders(ApiKey key)
        {
            var requestHeaders = new List<RequestHeader>();
            if (key != null && !string.IsNullOrEmpty(key.Value))
            {
                requestHeaders.Add(new RequestHeader { Name = key.Name, Value = key.Value });
            }

            return requestHeaders;
        }
    }
}