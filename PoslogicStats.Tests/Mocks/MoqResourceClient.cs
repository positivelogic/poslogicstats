﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;

using PoslogicStats.Common;
using PoslogicStats.Common.Clients;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Tests.Mocks
{
    internal class MoqFileResourceClient :
        Mock<IFileResourceClient>
    {
        public string GetAsyncResult = "";
        public MoqFileResourceClient()
        {
            Setup(m => m.GetAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>())).Returns(() =>
              {
                  return Task.FromResult(GetAsyncResult);
              });
        }
    }


    internal class MoqWebApiResourceClient :
       Mock<IWebApiResourceClient>
    {
        public string GetAsyncResult = "";
        public MoqWebApiResourceClient()
        {
            Setup(m => m.GetAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>())).Returns(() =>
            {
                return Task.FromResult(GetAsyncResult);
            });
        }
    }
}