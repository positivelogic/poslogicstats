﻿using System.IO;
using System.Threading.Tasks;

using NUnit.Framework;

using PoslogicStats.Common.Clients;
using PoslogicStats.Repositories.AlphaVantage;
using PoslogicStats.Repositories.Setting;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class AlphaVantageRepositoryTests
    {
        [Test]
        [Category("Integration")]
        public async Task Int_Call_GetDigitalCurrencyAsync()
        {
            using (var webResource = new WebApiClient())
            {
                var path = Directory.GetCurrentDirectory();
                var settingRepo = new SettingRepository(path);
                var fileResource = new FileSystemClient();

                var target = new AlphaVantageRepository(webResource, fileResource, settingRepo);

                var actual = await target.GetDigitalCurrencyAsync();

                Assert.IsNotNull(actual);
            }
        }

        [Test]
        [Category("Integration")]
        public async Task Int_Call_GetPhysicalCurrencyAsync()
        {
            using (var webResource = new WebApiClient())
            {
                var path = Directory.GetCurrentDirectory();
                var settingRepo = new SettingRepository(path);
                var fileResource = new FileSystemClient();

                var target = new AlphaVantageRepository(webResource, fileResource, settingRepo);

                var actual = await target.GetPhysicalCurrencyAsync();

                Assert.IsNotNull(actual);
            }
        }
    }
}