﻿using System;

namespace PoslogicStats.Common.Models.Quandl
{
    public class DatasetsV3
    {
        public Dataset[] datasets { get; set; }
        public Meta meta { get; set; }
    }

    public class Meta
    {
        public string query { get; set; }
        public int per_page { get; set; }
        public int current_page { get; set; }
        public object prev_page { get; set; }
        public int total_pages { get; set; }
        public int total_count { get; set; }
        public int next_page { get; set; }
        public int current_first_item { get; set; }
        public int current_last_item { get; set; }
    }

    public class Dataset
    {
        public int id { get; set; }
        public string dataset_code { get; set; }
        public string database_code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime refreshed_at { get; set; }
        public string newest_available_date { get; set; }
        public string oldest_available_date { get; set; }
        public string[] column_names { get; set; }
        public string frequency { get; set; }
        public string type { get; set; }
        public bool premium { get; set; }
        public int database_id { get; set; }
    }

}