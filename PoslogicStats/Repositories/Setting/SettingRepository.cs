﻿using System.IO;
using System.Reflection;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

using PoslogicStats.Common.Models;

namespace PoslogicStats.Repositories.Setting
{
    public interface ISettingRepository
    {
        Task<FileDetail> GetApiFileDetail();
        Task<string> GetDataLocation();
    }

    public class SettingRepository : ISettingRepository
    {
        readonly IConfigurationRoot _configuration;
        readonly string _basePath;
        public SettingRepository(string path = null)
        {
            _basePath = AssemblyDirectory;
            if (!string.IsNullOrEmpty(path))
            {
                _basePath = path;
            }

            var builder = new ConfigurationBuilder()
                .SetBasePath(_basePath)
                .AddJsonFile("Conf/appsettings.json", optional: true, reloadOnChange: true);

            _configuration = builder.Build();
        }

        public async Task<FileDetail> GetApiFileDetail()
        {
            var filename = _configuration.GetSection("settings").GetSection("apikey").GetSection("filename");
            var filepath = _configuration.GetSection("settings").GetSection("apikey").GetSection("filepath");

            var filedetail = new FileDetail { FileName = filename.Value, Path = filepath.Value };

            return await Task.FromResult(filedetail);
        }

        public async Task<string> GetDataLocation()
        {
            var dataLocation = $"{_basePath}/Data";
            return await Task.FromResult(dataLocation);
        }

        string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetEntryAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}