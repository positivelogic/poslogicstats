﻿namespace PoslogicStats.Common.Models.StatisticsNz
{
    public class TableCode
    {
        public string odatacontext { get; set; }
        public ValueItem[] value { get; set; }
    }

    public class ValueItem
    {
        public string IDANSWER_Code { get; set; }
        public string IDANSWER_Name { get; set; }
        public string IDCATEGORY_Code { get; set; }
        public string IDCATEGORY_Name { get; set; }
        public string IDMEASURES_Code { get; set; }
        public string IDMEASURES_Name { get; set; }
        public string IDYEAR_ENDED_JUNE_Code { get; set; }
        public string IDYEAR_ENDED_JUNE_Name { get; set; }
        public float Value { get; set; }
        public string tablecode { get; set; }
        public string Group { get; set; }
        public string Entity { get; set; }
    }
}