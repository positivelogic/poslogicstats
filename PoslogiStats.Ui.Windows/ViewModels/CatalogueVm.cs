﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Newtonsoft.Json;

using PoslogicStats.Common.Clients;
using PoslogicStats.Common.Models.StatisticsNz;
using System.Windows;
using PoslogiStats.Ui.Windows.Views;

namespace PoslogiStats.Ui.Windows.ViewModels
{
    public class CatalogueVm : BaseVm
    {
        public CatalogueVm()
        {
            Title = "StatisticNz Catalogue List";
        }

        public async Task GetCatalogue()
        {
            using (var resourceClient = new WebApiClient())
            {
                var stringJsonResult = await resourceClient.GetAsync("http://localhost", "/WebApi/api/values");
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                CatalogueItems = JsonConvert.DeserializeObject<IEnumerable<Value>>(stringJsonResult, settings);
                if (CatalogueItems != null && CatalogueItems.Count() > 0)
                {
                    SelectedCatalogueItem = CatalogueItems.FirstOrDefault();
                }
            }
        }

        IEnumerable<Value> _catalogueItems;
        public IEnumerable<Value> CatalogueItems
        {
            get { return _catalogueItems; }
            set
            {
                _catalogueItems = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Visibility");
            }
        }

        Value _selectedCatalogueItem;
        public Value SelectedCatalogueItem
        {
            get { return _selectedCatalogueItem; }
            set
            {
                _selectedCatalogueItem = value;
                NotifyPropertyChanged();
            }
        }

        public string Title { get; set; }

        public Visibility Visibility
        {
            get
            {
                if (CatalogueItems!= null && CatalogueItems.Count() > 0)
                    return Visibility.Visible;
                else
                    return Visibility.Hidden;
            }
        }

        public void ShowDetails()
        {
            var vm = new CatalogueDetailVm();
            vm.TableCode = SelectedCatalogueItem.TableCode;
            var view = new CatalogueItemDetailView(vm);
            view.ShowDialog();
        }
    }
}