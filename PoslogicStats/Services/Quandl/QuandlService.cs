﻿using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using PoslogicStats.Repositories.Quandl;
using PoslogicStats.Services.Credential;
using PoslogicStats.Common.Models.Quandl;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Services.Quandl
{
    public interface IQuandlService
    {
        Task<TimeSeries> GetTimeSeriesDataAsync(string databasecode, string datasetCode);
        Task<DatasetsV3> GetDatasetsAsync();
    }

    public class QuandlService : IQuandlService
    {
        readonly IQuandlRepository _quandlRepository;
        readonly ICredentialService _credentialService;
        const string provider = "Quandl";
        public QuandlService(IQuandlRepository quandlRepository, ICredentialService credentialService)
        {
            _quandlRepository = quandlRepository;
            _credentialService = credentialService;
        }

        public async Task<TimeSeries> GetTimeSeriesDataAsync(string databasecode, string datasetCode)
        {
            ApiKey key = await GetApiKey();

            List<RequestHeader> requestHeaders = SetRequestHeaders(key);

            var stringResult = await _quandlRepository.GetTimeSeriesDataAsync(databasecode, datasetCode, requestHeaders);
            var timeseriesData = Newtonsoft.Json.JsonConvert.DeserializeObject<TimeSeries>(stringResult);
            return timeseriesData;
        }

        public async Task<DatasetsV3> GetDatasetsAsync()
        {
            var stringResult = await _quandlRepository.GetDatasetsAsync();
            var timeseriesData = Newtonsoft.Json.JsonConvert.DeserializeObject<DatasetsV3>(stringResult);
            return timeseriesData;
        }
        private async Task<ApiKey> GetApiKey()
        {
            var apikey = "api_key";
            var keys = await _credentialService.GetApiKeyAsync();
            var key = keys.SingleOrDefault(m => m.Provider.Equals(provider) && m.Name.Equals(apikey));
            return key;
        }

        private static List<RequestHeader> SetRequestHeaders(ApiKey key)
        {
            var requestHeaders = new List<RequestHeader>();
            if (key != null && !string.IsNullOrEmpty(key.Value))
            {
                requestHeaders.Add(new RequestHeader { Name = key.Name, Value = key.Value });
            }

            return requestHeaders;
        }
    }
}