﻿namespace PoslogicStats.Common.Models
{
    public class RequestHeader
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}