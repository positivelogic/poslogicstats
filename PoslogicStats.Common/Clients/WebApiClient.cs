﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using PoslogicStats.Common;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Common.Clients
{
    public interface IWebApiResourceClient : IResourceClient { }

    public class WebApiClient : BaseDisposable, IWebApiResourceClient
    {
        HttpClient _httpClient;
        public WebApiClient() => _httpClient = new HttpClient();

        public async Task<string> GetAsync(string baseAddress, string path, IEnumerable<RequestHeader> requestHeaders = null)
        {
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.BaseAddress = new Uri(baseAddress);
            
            if (requestHeaders != null && requestHeaders.Count() > 0)
            {
                foreach (var requestHeader in requestHeaders)
                {
                    _httpClient.DefaultRequestHeaders.Add(requestHeader.Name, requestHeader.Value);
                }
            }

            var stringTask = _httpClient.GetStringAsync(path);

            var msg = await stringTask;

            return msg;
        }

        public Task<bool> PutAsync(string baseAddress, string path, string payload, IEnumerable<RequestHeader> requestHeaders = null)
        {
            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_httpClient != null)
                {
                    _httpClient.Dispose();
                }
            }

            base.Dispose(disposing);
        }
    }
}