﻿using Newtonsoft.Json;
using PoslogicStats.Common.Clients;
using PoslogicStats.Common.Models.StatisticsNz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PoslogiStats.Ui.Windows.ViewModels
{
    public class CatalogueDetailVm : BaseVm
    {
        public CatalogueDetailVm()
        {
            Title = "StatisticNz Catalogue Detail List";
        }

        public async Task GetCatalogueDetail()
        {
            using (var resourceClient = new WebApiClient())
            {
                var stringJsonResult = await resourceClient.GetAsync("http://localhost", $"/WebApi/api/values/{TableCode}");
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                CatalogueDetailItems = JsonConvert.DeserializeObject<IEnumerable<ValueItem>>(stringJsonResult, settings);
                if (CatalogueDetailItems != null && CatalogueDetailItems.Count() > 0)
                {
                    SelectedCatalogueDetailItem = CatalogueDetailItems.FirstOrDefault();
                }
            }
        }

        IEnumerable<ValueItem> _catalogueDetailItems;
        public IEnumerable<ValueItem> CatalogueDetailItems
        {
            get { return _catalogueDetailItems; }
            set
            {
                _catalogueDetailItems = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Visibility");
            }
        }

        ValueItem _selectedCatalogueDetailItem;
        public ValueItem SelectedCatalogueDetailItem
        {
            get { return _selectedCatalogueDetailItem; }
            set
            {
                _selectedCatalogueDetailItem = value;
                NotifyPropertyChanged();
            }
        }

        public string Title { get; set; }

        public Visibility Visibility
        {
            get
            {
                if (CatalogueDetailItems != null && CatalogueDetailItems.Count() > 0)
                    return Visibility.Visible;
                else
                    return Visibility.Hidden;
            }
        }

        public string TableCode { get; set; }
    }
}
