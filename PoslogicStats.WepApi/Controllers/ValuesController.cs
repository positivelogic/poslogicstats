﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using PoslogicStats.Services.StatisticsNz;
using Microsoft.Extensions.Logging;

namespace PoslogicStats.WepApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        IStatisticNzService _statisticNzService;
        private readonly ILogger<ValuesController> _logger;
        public ValuesController(ILogger<ValuesController> logger,IStatisticNzService statisticNzService)
        {
            _logger = logger;
            _statisticNzService = statisticNzService ?? throw new ArgumentNullException(nameof(statisticNzService));
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation(LoggingEvents.ListItems, "List items");
            var catalogue = await _statisticNzService.GetCatalogueAsync();
            if (catalogue.value == null)
            {
                _logger.LogWarning(LoggingEvents.ListItems, "List items NOT FOUND");
                return NotFound();
            }
            return new ObjectResult(catalogue.value);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting item {ID}", id);
            var result = await _statisticNzService.GetByTableCodeAsync(id);
            if (result.value == null)
            {
                _logger.LogWarning(LoggingEvents.GetItemNotFound, "GetById({ID}) NOT FOUND", id);
                return NotFound();
            }
            return new ObjectResult(result.value);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}