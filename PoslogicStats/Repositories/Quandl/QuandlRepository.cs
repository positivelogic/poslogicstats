﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

using PoslogicStats.Common.Models;
using PoslogicStats.Common.Clients;

namespace PoslogicStats.Repositories.Quandl
{
    public interface IQuandlRepository
    {
        Task<string> GetTimeSeriesDataAsync(string database_code, string dataset_code, IEnumerable<RequestHeader> requestHeaders, string return_format = "json");
        Task<string> GetDatasetsAsync();
        Task<bool> PutDatasetsAsync();
    }

    public class QuandlRepository : IQuandlRepository
    {
        readonly IWebApiResourceClient _resourceClient;
        readonly string _url;
        public QuandlRepository(IWebApiResourceClient resourceClient)
        {
            _resourceClient = resourceClient;
            _url = "https://www.quandl.com";
        }

        public async Task<string> GetTimeSeriesDataAsync(string database_code, string dataset_code, IEnumerable<RequestHeader> requestHeaders, string return_format = "json")
        {
            var requestheader = requestHeaders.FirstOrDefault();
            if (requestheader == null)
                throw new ArgumentException("Request headers not present");

            var api = $"api/v3/datasets/{database_code}/{dataset_code}/data.{return_format}?{requestheader.Name}={requestheader.Value}";
            return await _resourceClient.GetAsync(_url, api);
        }

        public async Task<string> GetDatasetsAsync()
        {
            var api = "api/v3/datasets";
            return await _resourceClient.GetAsync(_url, api);
        }

        public Task<bool> PutDatasetsAsync()
        {
            throw new NotImplementedException();
        }
    }
}