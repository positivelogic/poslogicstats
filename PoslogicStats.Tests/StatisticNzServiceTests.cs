﻿using System.Threading.Tasks;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;

using PoslogicStats.Repositories.StatisticsNz;
using PoslogicStats.Common.Clients;
using PoslogicStats.Services.StatisticsNz;
using PoslogicStats.Repositories.Credential;
using PoslogicStats.Services.Credential;
using PoslogicStats.Common.Models;

using PoslogicStats.Tests.Mocks.Repositories;
using PoslogicStats.Tests.Mocks.Services;
using PoslogicStats.Tests.Mocks;
using PoslogicStats.Repositories.Setting;
using System.IO;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class StatisticNzServiceTests
    {
        MoqStatisticNzRepository MoqStatisticNzRepository;
        MoqCredentialService MoqCredentialService;
        MoqWebApiResourceClient MoqWebApiResourceClient;
        IStatisticNzRepository StatisticNzRepository;

        [SetUp]
        public void Setup()
        {
            MoqWebApiResourceClient = new MoqWebApiResourceClient();
            MoqStatisticNzRepository = new MoqStatisticNzRepository();
            MoqCredentialService = new MoqCredentialService();
            StatisticNzRepository = new StatisticNzRepository(MoqWebApiResourceClient.Object);
        }

        [Test]
        [Category("Unit")]
        public async Task Call_GetCatalogueAsync_With_Moq_Repo()
        {
            MoqStatisticNzRepository.GetCatalogueResults = Properties.Resources.ResourceManager.GetString("statisticsnz_v1_odata_catalogue_data");

            var target = new StatisticNzService(MoqStatisticNzRepository.Object, MoqCredentialService.Object);

            var actual = await target.GetCatalogueAsync();

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.value.Length, 50);
        }

        [Test]
        [Category("Unit")]
        public async Task Call_GetCatalogueAsync_With_Real_Repo()
        {
            MoqWebApiResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("statisticsnz_v1_odata_catalogue_data");

            var target = new StatisticNzService(StatisticNzRepository, MoqCredentialService.Object);

            var actual = await target.GetCatalogueAsync();

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.value.Length, 50);
            MoqWebApiResourceClient.Verify(m => m.GetAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>()), Times.Once());
            MoqCredentialService.Verify(m => m.GetApiKeyAsync(), Times.Once());
        }


        //[Test]
        [Category("Integration")]
        public async Task Int_Call_GetCatalogueAsync_Result_Ok()
        {
            using (var resourceClient = new WebApiClient())
            {
                var path = Directory.GetCurrentDirectory();
                var setting = new SettingRepository(path);
                var resource = new FileSystemClient();
                var repo = new StatisticNzRepository(resourceClient);
                var credsRepo = new CredentialRepository(resource, setting);
                var credsService = new CredentialService(credsRepo);

                var target = new StatisticNzService(repo, credsService);

                var actual = await target.GetCatalogueAsync();

                Assert.IsNotNull(actual);
            }
        }

        //[Test]
        [Category("Integration")]
        public async Task Int_Call_GetByTableCodeAsync_Result_Ok()
        {
            using (var resourceClient = new WebApiClient())
            {
                var path = Directory.GetCurrentDirectory();
                var setting = new SettingRepository(path);
                var resource = new FileSystemClient();
                var repo = new StatisticNzRepository(resourceClient);
                var credsRepo = new CredentialRepository(resource, setting);
                var credsService = new CredentialService(credsRepo);

                var target = new StatisticNzService(repo, credsService);

                var actual = await target.GetByTableCodeAsync("TABLECODE327");

                Assert.IsNotNull(actual);
            }
        }
    }
}