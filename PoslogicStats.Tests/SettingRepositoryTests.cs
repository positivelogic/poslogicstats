﻿using System.Threading.Tasks;
using NUnit.Framework;

using PoslogicStats.Repositories.Setting;
using System.IO;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class SettingRepositoryTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public async Task Call_GetApiFileDetail()
        {
            var path = Directory.GetCurrentDirectory();
            var target = new SettingRepository(path);

            var actual = await target.GetApiFileDetail();

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.FileName, "ApiKeys.json");
        }
    }
}