﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;

using PoslogicStats.Repositories.Quandl;
using PoslogicStats.Services.Quandl;
using PoslogicStats.Services.Credential;
using PoslogicStats.Repositories.Credential;
using PoslogicStats.Common.Models;
using PoslogicStats.Repositories.Setting;
using PoslogicStats.Common.Clients;

using PoslogicStats.Tests.Mocks;
using PoslogicStats.Tests.Mocks.Repositories;
using PoslogicStats.Tests.Mocks.Services;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class QuandlServiceTests
    {
        MoqQuandlRepository MoqQuandlRepository;
        MoqCredentialService MoqCredentialService;
        MoqFileResourceClient MoqFileResourceClient;
        MoqWebApiResourceClient MoqWebApiResourceClient;
        IQuandlRepository QuandlRepository;

        [SetUp]
        public void Setup()
        {
            MoqFileResourceClient = new MoqFileResourceClient();
            MoqWebApiResourceClient = new MoqWebApiResourceClient();
            MoqQuandlRepository = new MoqQuandlRepository();

            MoqCredentialService = new MoqCredentialService();

            QuandlRepository = new QuandlRepository(MoqWebApiResourceClient.Object);
        }

        [Test]
        [Category("Unit")]
        public async Task GetTimeSeriesDataAsync_With_Moq_Repo()
        {
            MoqQuandlRepository.GetTimeSeriesDataAsyncResults = Properties.Resources.ResourceManager.GetString("quandl_v3_api_timeseries_data");

            var target = new QuandlService(MoqQuandlRepository.Object, MoqCredentialService.Object);

            var actual = await target.GetTimeSeriesDataAsync("WIKI", "FB");

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.dataset_data.column_names.Length, 13);
            Assert.AreEqual(actual.dataset_data.data.Length, 1385);
            Assert.AreEqual(actual.dataset_data.frequency, "daily");
            Assert.AreEqual(actual.dataset_data.start_date, "2012-05-18");
            Assert.AreEqual(actual.dataset_data.end_date, "2017-11-17");
            MoqQuandlRepository.Verify(m=>m.GetTimeSeriesDataAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>(), It.IsAny<string>()), Times.Once());
        }

        [Test]
        [Category("Unit")]
        public async Task GetTimeSeriesDataAsync_With_Real_Repo_Valid_ApiKey_Provider_And_Name()
        {
            MoqWebApiResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("quandl_v3_api_timeseries_data");
            MoqCredentialService.GetApiKeyAsyncResults = new List<ApiKey>() { new ApiKey { Name = "api_key", Provider = "Quandl", Value = "Value1" } };


            var target = new QuandlService(QuandlRepository, MoqCredentialService.Object);

            var actual = await target.GetTimeSeriesDataAsync("WIKI", "FB");

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.dataset_data.column_names.Length, 13);
            Assert.AreEqual(actual.dataset_data.data.Length, 1385);
            Assert.AreEqual(actual.dataset_data.frequency, "daily");
            Assert.AreEqual(actual.dataset_data.start_date, "2012-05-18");
            Assert.AreEqual(actual.dataset_data.end_date, "2017-11-17");
            MoqCredentialService.Verify(m => m.GetApiKeyAsync(), Times.Once());
        }

        [Test]
        [Category("Unit")]
        public void GetTimeSeriesDataAsync_With_Real_Repo_InValid_ApiKey_Provider_And_Valid_Name_Throws()
        {
            MoqWebApiResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("quandl_v3_api_timeseries_data");
            MoqCredentialService.GetApiKeyAsyncResults = new List<ApiKey>() { new ApiKey { Name = "api_key", Provider = "InvalidProvider", Value = "Value1" } };

            var target = new QuandlService(QuandlRepository, MoqCredentialService.Object);

            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await target.GetTimeSeriesDataAsync("WIKI", "FB");
            });

            MoqCredentialService.Verify(m => m.GetApiKeyAsync(), Times.Once());
        }
        [Test]
        [Category("Unit")]
        public void GetTimeSeriesDataAsync_With_Real_Repo_InValid_ApiKey_Provider_And_InValid_Name_Throws()
        {
            MoqWebApiResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("quandl_v3_api_timeseries_data");
            MoqCredentialService.GetApiKeyAsyncResults = new List<ApiKey>() { new ApiKey { Name = "InvalidName", Provider = "InvalidProvider", Value = "Value1" } };

            var target = new QuandlService(QuandlRepository, MoqCredentialService.Object);

            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await target.GetTimeSeriesDataAsync("WIKI", "FB");
            });

            MoqCredentialService.Verify(m => m.GetApiKeyAsync(), Times.Once());
        }

        [Test]
        [Category("Unit")]
        public async Task GetDatasetsAsync_With_Moq_Repo()
        {
            MoqQuandlRepository.GetDatasetsAsyncResults = Properties.Resources.ResourceManager.GetString("quandl_v3_api_datasets_data");

            var target = new QuandlService(MoqQuandlRepository.Object, MoqCredentialService.Object);

            var actual = await target.GetDatasetsAsync();

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.datasets.Length, 100);
            MoqQuandlRepository.Verify(m => m.GetDatasetsAsync(), Times.Once());
        }

        [Test]
        [Category("Unit")]
        public async Task GetDatasetsAsync_With_Real_Repo()
        {
            MoqWebApiResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("quandl_v3_api_datasets_data");

            var target = new QuandlService(QuandlRepository, MoqCredentialService.Object);

            var actual = await target.GetDatasetsAsync();

            Assert.IsNotNull(actual);
            Assert.AreEqual(actual.datasets.Length, 100);
            MoqCredentialService.Verify(m => m.GetApiKeyAsync(), Times.Never());
        }


        [Category("Integration")]
        public async Task Integration_Call_GetTimeSeriesDataAsync_Result_Model_Ok()
        {
            using (var resourceClient = new WebApiClient())
            {
                var setting = new SettingRepository();
                var resource = new FileSystemClient();
                var repo = new QuandlRepository(resourceClient);
                var credsRepo = new CredentialRepository(resource, setting);
                var credsService = new CredentialService(credsRepo);

                var target = new QuandlService(repo, credsService);

                var actual = await target.GetTimeSeriesDataAsync("WIKI", "FB");

                Assert.IsNotNull(actual);
            }
        }

        [Category("Integration")]
        public async Task Integration_Call_GetDatasetsAsync_Result_Model_Ok()
        {
            using (var resourceClient = new WebApiClient())
            {
                var setting = new SettingRepository();
                var resource = new FileSystemClient();
                var repo = new QuandlRepository(resourceClient);
                var credsRepo = new CredentialRepository(resource, setting);
                var credsService = new CredentialService(credsRepo);

                var target = new QuandlService(repo, credsService);

                var actual = await target.GetDatasetsAsync();

                Assert.IsNotNull(actual);
            }
        }
    }
}