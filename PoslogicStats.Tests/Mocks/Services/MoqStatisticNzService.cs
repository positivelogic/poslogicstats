﻿using System.Threading.Tasks;
using Moq;

using PoslogicStats.Services.StatisticsNz;
using PoslogicStats.Common.Models.StatisticsNz;

namespace PoslogicStats.Tests.Mocks.Services
{
    internal class MoqStatisticNzService
        : Mock<IStatisticNzService>
    {
        public Catalogue GetCatalogueAsyncResults = new Catalogue();
        public MoqStatisticNzService()
        {
            Setup(m => m.GetCatalogueAsync()).Returns(() =>
            {
                return Task.FromResult(GetCatalogueAsyncResults);
            });
        }
    }
}