﻿namespace PoslogicStats.Common.Models.AlphaVantage
{
    public enum DigitalCurrencyFunction
    {
        DIGITAL_CURRENCY_INTRADAY,
        DIGITAL_CURRENCY_DAILY,
        DIGITAL_CURRENCY_WEEKLY,
        DIGITAL_CURRENCY_MONTHLY
    }
}