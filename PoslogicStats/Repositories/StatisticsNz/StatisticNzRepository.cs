﻿using System.Collections.Generic;
using System.Threading.Tasks;

using PoslogicStats.Common.Models;
using PoslogicStats.Common.Clients;

namespace PoslogicStats.Repositories.StatisticsNz
{
    public interface IStatisticNzRepository
    {
        Task<string> GetCatalogueAsync(IEnumerable<RequestHeader> requestHeaders);
        Task<string> GetByTableCodeAsync(string tableCode, IEnumerable<RequestHeader> requestHeaders);
    }

    public class StatisticNzRepository : IStatisticNzRepository
    {
        readonly IWebApiResourceClient _resourceClient;
        readonly string _url;

        public StatisticNzRepository(IWebApiResourceClient resourceClient)
        {
            _resourceClient = resourceClient;
            _url = "https://statisticsnz.azure-api.net";
        }

        public async Task<string> GetCatalogueAsync(IEnumerable<RequestHeader> requestHeaders)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            // Request parameters
            //queryString["$expand"] = "{string}";
            //queryString["$filter"] = "{string}";
            //queryString["$select"] = "{string}";
            //queryString["$orderby"] = "{string}";
            //queryString["$top"] = "{integer}";
            //queryString["$skip"] = "{integer}";
            //queryString["$count"] = "{boolean}";

            var api = $"nzdotstat/v1.0/odata/Catalogue?{queryString}";
            return await _resourceClient.GetAsync(_url, api, requestHeaders);
        }

        public async Task<string> GetByTableCodeAsync(string tableCode, IEnumerable<RequestHeader> requestHeaders)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            // Request parameters
            //queryString["$expand"] = "{string}";
            //queryString["$filter"] = "{string}";
            //queryString["$select"] = "{string}";
            //queryString["$orderby"] = "{string}";
            //queryString["$top"] = "{integer}";
            //queryString["$skip"] = "{integer}";
            //queryString["$count"] = "{boolean}";

            var api = $"nzdotstat/v1.0/odata/{tableCode}?{queryString}";
            return await _resourceClient.GetAsync(_url, api, requestHeaders);
        }
    }
}