﻿using System.Threading.Tasks;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;

using PoslogicStats.Repositories.Credential;
using PoslogicStats.Services.Credential;
using PoslogicStats.Common.Models;
using PoslogicStats.Repositories.Setting;
using PoslogicStats.Common.Clients;


using PoslogicStats.Tests.Mocks.Repositories;
using PoslogicStats.Tests.Mocks;

namespace PoslogicStats.Tests
{
    [TestFixture]
    public class CredentialServiceTests
    {
        MoqCredentialRepository MoqCredentialRepository;
        MoqFileResourceClient MoqResourceClient;
        MoqSettingRepository MoqSettingRepository;
        ICredentialRepository CredentialRepository;
        [SetUp]
        public void Setup()
        {
            MoqCredentialRepository = new MoqCredentialRepository();
            MoqResourceClient = new MoqFileResourceClient();
            MoqSettingRepository = new MoqSettingRepository();
            CredentialRepository = new CredentialRepository(MoqResourceClient.Object, MoqSettingRepository.Object);
        }

        [Test]
        [Category("Unit")]
        public async Task Call_GetApiKeyAsync_With_Real_Repo()
        {
            MoqResourceClient.GetAsyncResult = Properties.Resources.ResourceManager.GetString("apikeys");
            var target = new CredentialService(CredentialRepository);

            var actual = await target.GetApiKeyAsync();

            Assert.IsNotNull(actual);
            MoqResourceClient.Verify(m => m.GetAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>()), Times.Once());
            MoqSettingRepository.Verify(m => m.GetApiFileDetail(), Times.Once());
        }

        [Test]
        [Category("Unit")]
        public async Task Call_GetApiKeyAsync_With_Moq_Repo()
        {
            MoqCredentialRepository.GetApiKeysResults = Properties.Resources.ResourceManager.GetString("apikeys");
            var target = new CredentialService(MoqCredentialRepository.Object);

            var actual = await target.GetApiKeyAsync();

            Assert.IsNotNull(actual);
            MoqCredentialRepository.Verify(m => m.GetApiKeysAsync(), Times.Once());
        }


        [Category("Integration")]
        public async Task Int_Call_GetApiKeyAsync_Result_Ok()
        {
            using (var resource = new FileSystemClient())
            {
                var setting = new SettingRepository();
                var repo = new CredentialRepository(resource, setting);

                var target = new CredentialService(repo);

                var actual = await target.GetApiKeyAsync();

                Assert.IsNotNull(actual);
            }
        }
    }
}