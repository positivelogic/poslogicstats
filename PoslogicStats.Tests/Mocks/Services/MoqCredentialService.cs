﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;

using PoslogicStats.Services.Credential;
using PoslogicStats.Common.Models;

namespace PoslogicStats.Tests.Mocks.Services
{
    internal class MoqCredentialService
        : Mock<ICredentialService>
    {
        public IEnumerable<ApiKey> GetApiKeyAsyncResults = new List<ApiKey>();
        public MoqCredentialService()
        {
            Setup(m => m.GetApiKeyAsync()).Returns(() =>
            {
                return Task.FromResult(GetApiKeyAsyncResults);
            });
        }
    }
}