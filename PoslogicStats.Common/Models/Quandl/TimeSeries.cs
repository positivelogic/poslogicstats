﻿namespace PoslogicStats.Common.Models.Quandl
{
    public class TimeSeries
    {
        public Dataset_Data dataset_data { get; set; }
    }

    public class Dataset_Data
    {
        public object limit { get; set; }
        public object transform { get; set; }
        public object column_index { get; set; }
        public string[] column_names { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string frequency { get; set; }
        public object[][] data { get; set; }
        public object collapse { get; set; }
        public object order { get; set; }
    }
}