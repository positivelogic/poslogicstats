﻿using System.Windows;
using MahApps.Metro.Controls;
using PoslogiStats.Ui.Windows.ViewModels;
using System;

namespace PoslogiStats.Ui.Windows.Views
{
    /// <summary>
    /// Interaction logic for CatalogueView.xaml
    /// </summary>
    public partial class CatalogueItemDetailView : MetroWindow
    {
        readonly CatalogueDetailVm _catalogueDetailVm;
        public CatalogueItemDetailView()
            :this(new CatalogueDetailVm())
        {
        }

        public CatalogueItemDetailView(CatalogueDetailVm catalogueDetailVm)
        {
            InitializeComponent();
            _catalogueDetailVm = catalogueDetailVm;
            DataContext = _catalogueDetailVm;
        }

        private async void ButtonGetData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await _catalogueDetailVm.GetCatalogueDetail();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}