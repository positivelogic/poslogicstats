﻿namespace PoslogicStats.Common.Models.StatisticsNz
{
    public class Catalogue
    {
        public string odatacontext { get; set; }
        public Value[] value { get; set; }
    }

    public class Value
    {
        public string Id { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Description { get; set; }
        public int TableCodeID { get; set; }
        public string TableCode { get; set; }
        public string Group { get; set; }
        public string Entity { get; set; }
    }
}