﻿using System.Threading.Tasks;

using PoslogicStats.Repositories.Setting;
using PoslogicStats.Common.Clients;

namespace PoslogicStats.Repositories.Credential
{
    public interface ICredentialRepository
    {
        Task<string> GetApiKeysAsync();
        Task<bool> PutApiKeysAsync(string payload);
    }

    public class CredentialRepository : ICredentialRepository
    {
        readonly IFileResourceClient _resourceClient;
        readonly ISettingRepository _settingRepository;
        public CredentialRepository(IFileResourceClient resourceClient, ISettingRepository settingRepository)
        {
            _resourceClient = resourceClient;
            _settingRepository = settingRepository;
        }

        public async Task<string> GetApiKeysAsync()
        {
            var fileDetail = await _settingRepository.GetApiFileDetail();
            return await _resourceClient.GetAsync(fileDetail.Path, fileDetail.FileName);
        }

        public async Task<bool> PutApiKeysAsync(string payload)
        {
            var fileDetail = await _settingRepository.GetApiFileDetail();
            return await _resourceClient.PutAsync(fileDetail.Path, fileDetail.FileName, payload);
        }
    }
}