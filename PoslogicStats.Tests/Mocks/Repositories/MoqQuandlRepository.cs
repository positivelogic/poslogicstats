﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;

using PoslogicStats.Common.Models;
using PoslogicStats.Repositories.Quandl;

namespace PoslogicStats.Tests.Mocks.Repositories
{
    internal class MoqQuandlRepository :
        Mock<IQuandlRepository>
    {
        public string GetDatasetsAsyncResults = "";
        public string GetTimeSeriesDataAsyncResults = "";
        public MoqQuandlRepository()
        {
            Setup(m => m.GetDatasetsAsync()).Returns(()=>
            {
                return Task.FromResult(GetDatasetsAsyncResults);
            });

            Setup(m => m.GetTimeSeriesDataAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IEnumerable<RequestHeader>>(), It.IsAny<string>())).Returns(() =>
            {
                return Task.FromResult(GetTimeSeriesDataAsyncResults);
            });
        }
    }
}