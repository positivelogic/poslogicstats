﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PoslogicStats.Common.Models;
using PoslogicStats.Common.Models.AlphaVantage;
using PoslogicStats.Repositories.AlphaVantage;
using PoslogicStats.Services.Credential;

namespace PoslogicStats.Services.AlphaVantage
{
    public interface IAlphaVantageService
    {
        Task<string> GetDailyDigitalCryptoCurrenciesAsync(string symbol, string market);
    }

    public class AlphaVantageService
    {
        readonly IAlphaVantageRepository _alphaVantageRepository;
        readonly ICredentialService _credentialService;
        const string provider = "AlphaVantage";
        public AlphaVantageService(IAlphaVantageRepository alphaVantageRepository, ICredentialService credentialService)
        {
            _alphaVantageRepository = alphaVantageRepository;
            _credentialService = credentialService;
        }

        public async Task<string> GetDailyDigitalCryptoCurrenciesAsync(string symbol, string market)
        {
            ApiKey key = await GetApiKey();
            List<RequestHeader> requestHeaders = SetRequestHeaders(key);

            var jsonStringResult = await _alphaVantageRepository.GetDigitalCryptoCurrenciesAsync(requestHeaders, DigitalCurrencyFunction.DIGITAL_CURRENCY_DAILY, symbol, market);



            return jsonStringResult;
        }

        private async Task<ApiKey> GetApiKey()
        {
            var apikey = "apikey";
            var keys = await _credentialService.GetApiKeyAsync();
            var key = keys.SingleOrDefault(m => m.Provider.Equals(provider) && m.Name.Equals(apikey));
            return key;
        }

        private static List<RequestHeader> SetRequestHeaders(ApiKey key)
        {
            var requestHeaders = new List<RequestHeader>();
            if (key != null && !string.IsNullOrEmpty(key.Value))
            {
                requestHeaders.Add(new RequestHeader { Name = key.Name, Value = key.Value });
            }

            return requestHeaders;
        }
    }
}