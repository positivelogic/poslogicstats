﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Moq;

using PoslogicStats.Common.Models;
using PoslogicStats.Repositories.StatisticsNz;

namespace PoslogicStats.Tests.Mocks.Repositories
{
    internal class MoqStatisticNzRepository :
        Mock<IStatisticNzRepository>
    {
        public string GetCatalogueResults = "";
        public MoqStatisticNzRepository()
        {
            Setup(m => m.GetCatalogueAsync(It.IsAny<IEnumerable<RequestHeader>>())).Returns(() =>
            {
                return Task.FromResult(GetCatalogueResults);
            });
        }
    }
}