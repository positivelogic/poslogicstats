﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

using PoslogicStats.Common.Models;

namespace PoslogicStats.Common.Clients
{
    public interface IFileResourceClient : IResourceClient { }

    public class FileSystemClient : BaseDisposable, IFileResourceClient
    {
        public async Task<string> GetAsync(string baseAddress, string path, IEnumerable<RequestHeader> requestHeaders = null)
        {
            var task = Task.Run(() =>
                {
                    var file = Path.Combine(baseAddress, path);
                    var stringTask = File.ReadAllText(file);
                    return stringTask;
                });

            return await task;
        }

        public async Task<bool> PutAsync(string baseAddress, string path, string payload, IEnumerable<RequestHeader> requestHeaders = null)
        {
            var task = Task.Run(() =>
            {
                var file = Path.Combine(baseAddress, path);
                File.WriteAllText(file, payload);
                return true;
            });

            return await task;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}